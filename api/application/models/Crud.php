<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *  All masterDB related CRUD(Create, Read, Update, Delete) operations are available here.
 * 
 * @author Harishkumar <reddy.startupindia@gmail.com>
 * 
 */
class Crud extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function create($table, $data) {
        return $this->db->insert($table, $data);
    }

    public function create_batch($table, $dataSet) {
        return $this->db->insert_batch($table, $dataSet);
    }

    public function read($table, $where = "", $fields = "", $start = 0, $limit = 0, $orderby = array(), $groupby = array()) {
        if (!empty($fields))
            $this->db->select($fields);


        if (!empty($where)) {
            if (is_array($where)) {
                foreach ($where as $key => $value) {
                    if (is_array($value))
                        $this->db->where_in($key, $value);
                    else
                        $this->db->where($key, $value);
                }
            } else {
                $this->db->where_in($where);
            }
        }

        if ($limit > 0)
            $this->db->limit($start, $limit);

        if (!empty($orderby)){
			if (is_array($orderby)) {
                foreach ($orderby as $key => $value) {
					$this->db->order_by($key, $value);
				}
		}
        if (!empty($groupby)){
			if (is_array($orderby))
				$this->db->group_by(implode(", ",$groupby));
			else
				$this->db->group_by($groupby);
		}

        return $this->db->get($table);
    }
	
	public function runQuery($query) {
        return $this->db->query($query);
    }

    public function getNumRows($table) {
        $this->db->select('count(id) as num_rows');
        return $this->db->get($table)->result();
    }

    public function update($table, $where, $data) {
        return $this->db->update($table, $data, $where);
    }

    public function delete($table, $where) {
        return $this->db->delete($table, $where);
    }

    public function getFields($table) {
        return $this->db->list_fields($table);
    }

    public function getTables() {
        return $this->db->query('show table status');
    }

}
