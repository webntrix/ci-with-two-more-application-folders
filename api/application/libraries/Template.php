<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Template {

    var $template_data = array();

    /**
     * By using set methoad, set the variabes inside template
     * 
     * @param [string/array] $variable pass the variable name as string or pass set of variables in the form of array.
     * @param [string][optional] $value pass the varlue of the variable as string when we pass the variable name as string.
     * 
     * @example if we want to set individuall variables. <br>$this->template->set('title', "My Application"); or we pass multiples at once like: <br>$variables = array("title" => "My Application", "taglin" => "my application tagline" ); <br>$this->template->set($variables);
     */
    function set($variable, $value = "") {
        if (is_array($variable)) {
            foreach ($variable as $key => $value) {
                $this->template_data[$key] = $value;
            }
        } else {
            $this->template_data[$variable] = $value;
        }
    }
    
    function load($view = '', $view_data = array(), $return = FALSE) {
        $this->CI = & get_instance();
        $template = $this->CI->config->item("app_config")["template"];
        $this->set('contents', $this->CI->load->view($view, $view_data, TRUE));
        return $this->CI->load->view("templates/" . $template . "/" . $template, $this->template_data, $return);
    }

}
