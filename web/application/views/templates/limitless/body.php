<body> 
    <?php include_once dirname(__FILE__) . '/block_header.php'; ?> 
    <?php include_once dirname(__FILE__) . '/block_navbar.php'; ?>
    <div class="page-header">
        <?php
            if (!empty($ci->config->item("app_config")['admin']['bredcrum']) && $ci->config->item("app_config")['admin']['bredcrum'] === 1)
                include_once dirname(__FILE__) . "/block_page_header.php";
            ?>
    </div>
    
    <!-- Start View -->
    <!-- Page container -->
    <div class="page-container">
        <!-- Page content -->
        <div class="page-content">
            <!-- Main content -->
            <div class="content-wrapper">
                <?php echo $contents ?>
            </div>
        </div>
    </div>    
    <!-- End View -->

    <?php include_once dirname(__FILE__) . '/block_footer.php'; ?> 
    <?php include_once dirname(__FILE__) . '/block_js.php'; ?>

</body>
