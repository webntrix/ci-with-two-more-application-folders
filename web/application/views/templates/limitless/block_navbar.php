<!-- Second navbar -->
<div class="navbar navbar-default" id="navbar-second">
    <ul class="nav navbar-nav no-border visible-xs-block">
        <li><a class="text-center collapsed" data-toggle="collapse" data-target="#navbar-second-toggle"><i class="icon-menu7"></i></a></li>
    </ul>

    <div class="navbar-collapse collapse" id="navbar-second-toggle">
        <ul class="nav navbar-nav">
            <?php
            foreach ($ci->config->item("app_config")['admin']['menu'] as $menu => $menuprops) {
                if ($menuprops['status'] === 1) {
                    if (empty($menuprops['sub_menu'])): //for creating main menu
                        ?>
                        <li><a href="<?php echo base_url($menuprops['link']); ?>"><i class="<?php echo $menuprops['icon']; ?> position-left"></i> <?php echo ucfirst($menu); ?></a></li>
                        <?php
                    else:  //for creating main menu with 2nd level
                        ?>
                        <li class="dropdown">
                            <a href="<?php echo base_url($menuprops['link']); ?>" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="<?php echo $menuprops['icon']; ?> position-left"></i> <?php echo ucfirst($menu); ?> <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu width-250">                            
                                <?php
                                foreach ($menuprops['sub_menu'] as $menu2 => $m2props) :
                                    if ($menuprops['status'] === 1) :
                                        if (empty($m2props['sub_menu'])): //for creating 2nd level menu
                                            ?>
                                            <li><a href="<?php echo base_url($m2props['link']); ?>"><i class="<?php echo $m2props['icon']; ?> position-left"></i> <?php echo ucfirst($menu2); ?></a></li>
                                            <?php
                                        else: //for creating 2nd level menu with rd level
                                            ?>
                                            <li class="dropdown-submenu"> 
                                                <a href="#"><i class="icon-task"></i>  <?php echo ucfirst($menu2); ?></a>
                                                <ul class="dropdown-menu width-250">
                                                    <?php
                                                    foreach ($m2props['sub_menu'] as $menu3 => $m3props) : //for creating 3rd level memu
                                                        if ($m3props['status'] === 1) :
                                                            ?>
                                                            <li><a href="<?php echo base_url($m3props['link']); ?>"><i class="<?php echo $m3props['icon']; ?> position-left"></i> <?php echo ucfirst($menu3); ?></a></li>
                                                            <?php
                                                        endif;
                                                    endforeach;
                                                    ?>
                                                </ul>
                                            </li>
                                        <?php
                                        endif;
                                    endif;
                                endforeach;
                                ?>

                            </ul>
                        </li>
                    <?php
                    endif;
                }
            }
            ?>

        </ul>

        <ul class="nav navbar-nav navbar-right">
            <?php
            if (!empty($ci->config->item("app_config")['admin']['tray_catlog']) && $ci->config->item("app_catlog")['admin']['tray_catlog'] === 1)
                include_once dirname(__FILE__) . "/block_tray_catlog.php";
            ?>
        </ul>
    </div>
</div>
<!-- /second navbar -->