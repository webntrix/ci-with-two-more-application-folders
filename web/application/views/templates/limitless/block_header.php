<!-- Main navbar -->
<div class="navbar navbar-inverse">
    <div class="navbar-header">
        <?php if (!empty($ci->config->item("app_config")['logo'])): ?>
            <a class="navbar-brand" href="/"><img src="<?php echo base_url("assets/images/" . $ci->config->item("app_config")['logo']) ?>" alt="<?php echo base_url("assets/images/" . $ci->config->item("app_config")['title']) ?>"></a>
        <?php else: ?>
            <a class="navbar-brand" href="/"><h1 class="logo"><?php echo $ci->config->item("app_config")['title'] ?></h1></a>
        <?php endif; ?>
        <ul class="nav navbar-nav pull-right visible-xs-block">
            <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
        </ul>
    </div>

    <div class="navbar-collapse collapse" id="navbar-mobile">
        <!--<p class="navbar-text"><span class="label bg-success-400">Online</span></p>-->

        <ul class="nav navbar-nav navbar-right">
            <?php
            if (!empty($ci->config->item("app_config")['admin']['tray_tasks']) && $ci->config->item("app_config")['admin']['tray_tasks'] === 1)
                include_once dirname(__FILE__) . "/block_tray_tasks.php";
            ?>

            <?php
            if (!empty($ci->config->item("app_config")['admin']['tray_online_users']) && $ci->config->item("app_config")['tray_online_users'] === 1)
                include_once dirname(__FILE__) . "/block_tray_online_users.php";
            ?>
            
            <?php
            if (!empty($ci->config->item("app_config")['admin']['tray_languages']) && $ci->config->item("app_config")['admin']['tray_taskslanguages'] === 1)
                include_once dirname(__FILE__) . "/block_tray_languages.php";
            ?>

            <?php
            if (!empty($ci->config->item("app_config")['admin']['tray_notifications']) && $ci->config->item("app_config")['admin']['tray_notifications'] === 1)
                include_once dirname(__FILE__) . "/block_tray_notifications.php";
            ?>
            
            <?php
            if (!empty($ci->config->item("app_config")['admin']['tray_notifications']) && $ci->config->item("app_config")['admin']['tray_notifications'] === 1)
                include_once dirname(__FILE__) . "/block_tray_notifications.php";
            ?>
            
            <?php
            if (!empty($ci->config->item("app_config")['admin']['tray_user']) && $ci->config->item("app_config")['admin']['tray_user'] === 1)
                include_once dirname(__FILE__) . "/block_tray_user.php";
            ?>
            
        </ul>
    </div>
</div>
<!-- /main navbar -->