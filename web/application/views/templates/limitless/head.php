    <head>
        <?php include_once dirname(__FILE__). '/block_meta.php'; ?>
        <title><?php echo $ci->config->item("app_config")['title'] ?>::<?php  echo (isset($title))? $title:"" ?></title>
        <link rel="shortcut icon" href="<?php echo (!empty($ci->config->item("app_config")['icon']))? base_url("assets/images/".$ci->config->item("app_config")['icon']):"favicon.ico" ?>"/>
        <?php include_once dirname(__FILE__). '/block_css.php'; ?>
        
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
