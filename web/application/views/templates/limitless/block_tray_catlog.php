
<li>
    <a href="changelog.html">
        <i class="icon-history position-left"></i>
        Changelog
        <span class="label label-inline position-right bg-success-400">1.3</span>
    </a>
</li>

<li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <i class="icon-cog3"></i>
        <span class="visible-xs-inline-block position-right">Share</span>
        <span class="caret"></span>
    </a>

    <ul class="dropdown-menu dropdown-menu-right">
        <li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
        <li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
        <li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
        <li class="divider"></li>
        <li><a href="#"><i class="icon-gear"></i> All settings</a></li>
    </ul>
</li>