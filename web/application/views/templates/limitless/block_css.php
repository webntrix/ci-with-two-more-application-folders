<!-- Global stylesheets -->
<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
<link href="<?php echo base_url("assets/css/icons/icomoon/styles.css"); ?>" rel="stylesheet" type="text/css">
<link href="<?php echo base_url("assets/css/bootstrap.css"); ?>" rel="stylesheet" type="text/css">
<link href="<?php echo base_url("assets/css/core.css"); ?>" rel="stylesheet" type="text/css">
<link href="<?php echo base_url("assets/css/components.css"); ?>" rel="stylesheet" type="text/css">
<link href="<?php echo base_url("assets/css/colors.css"); ?>" rel="stylesheet" type="text/css">
<link href="<?php echo base_url("assets/css/app.css"); ?>" rel="stylesheet" type="text/css">
<!-- /global stylesheets -->

<!-- Module level stylesheets -->
<?php
if (is_dir(APPPATH . "modules/" . $segments[1] . "/css/")) {
    $css = scandir(APPPATH . "modules/" . $segments[1] . "/css/");
} else {
    $css = scandir(APPPATH . "modules\\" . $segments[1] . "\css\\");
}
unset($css[0]);
unset($css[1]);
foreach ($css as $key => $file) : ?>
    <link href="<?php echo base_url("application/modules/" . $segments[1] . "/css/". $file); ?>" rel="stylesheet" type="text/css">
<?php endforeach; ?>

