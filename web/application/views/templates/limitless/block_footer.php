<!-- Footer -->
<div class="footer text-muted">
    <hr class="m-5"/>
    &copy; <a href="/"><?php echo $ci->config->item("app_config")['copyright']; ?></a>
</div>
<!-- /footer -->