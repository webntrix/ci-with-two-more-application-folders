<!-- Core JS files -->
<script type="text/javascript" src="<?php echo base_url("assets/js/core/angular.min.js"); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/js/plugins/loaders/pace.min.js"); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/js/core/libraries/jquery.min.js"); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/js/core/libraries/bootstrap.min.js"); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/js/plugins/loaders/blockui.min.js"); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/js/plugins/ui/nicescroll.min.js"); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/js/plugins/ui/drilldown.js"); ?>"></script>
<!-- /core JS files -->

<!-- Theme JS files -->
<!--
<script type="text/javascript" src="<?php echo base_url("assets/js/plugins/visualization/d3/d3.min.js"); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/js/plugins/visualization/d3/d3_tooltip.js"); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/js/plugins/forms/styling/switchery.min.js"); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/js/plugins/forms/styling/uniform.min.js"); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/js/plugins/forms/selects/bootstrap_multiselect.js"); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/js/plugins/ui/moment/moment.min.js"); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/js/plugins/pickers/daterangepicker.js"); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/js/plugins/ui/headroom/headroom.min.js"); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/js/plugins/ui/headroom/headroom_jquery.min.js"); ?>"></script>
-->

<script type="text/javascript" src="<?php echo base_url("assets/js/core/app.js"); ?>"></script>
<!--<script type="text/javascript" src="<?php echo base_url("assets/js/pages/dashboard.js"); ?>"></script>-->
<script type="text/javascript" src="<?php echo base_url("assets/js/pages/layout_navbar_secondary_fixed.js"); ?>"></script>
<!-- /theme JS files -->

<!-- module level js -->
<!--<script type="text/javascript" src="<?php echo base_url("modules/" . $segments[1] . "/js/action.js"); ?>" ></script>-->

<?php

if (is_dir(APPPATH . "modules/" . $segments[1] . "/js/")) {
    $js = scandir(APPPATH . "modules/" . $segments[1] . "/js/");
} else {
    $js = scandir(APPPATH . "modules\\" . $segments[1] . "\js\\");
}
unset($js[0]);
unset($js[1]);
foreach ($js as $key => $file) :
    ?>
    <script type="text/javascript" src="<?php echo base_url("application/modules/" . $segments[1] . "/js/" . $file); ?>" ></script>
<?php endforeach; ?>
