<!DOCTYPE html>
<html>
    <?php
    $ci = &get_instance();
    if (!isset($segments)) {
        $segments = $ci->uri->rsegment_array();
    } else {
        $segments = $ci->uri->segment_array();
    }
    require_once dirname(__FILE__) . '/head.php';
    require_once dirname(__FILE__) . '/body.php';
    ?>
</html>
