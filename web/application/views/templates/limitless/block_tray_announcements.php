<li class="dropdown notifications-menu" id="try_anns">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <i class="fa fa-bullhorn"></i>
        <span class="label label-warning" id="tray_anns_count"></span>
    </a>
    <ul class="dropdown-menu">
        <li class="header" id="tray_anns_header"></li>
        <li>
            <!-- inner menu: contains the actual data -->
            <ul class="menu" id="tray_anns_list">
                
            </ul>
        </li>
        <li class="footer"><a href="/announcements"><span class="text-blue">All Announcements</span></a></li>
        <li class="footer"><a href="/bulletins"><span class="text-blue">All Bulletins</span></a></li>
    </ul>
</li>