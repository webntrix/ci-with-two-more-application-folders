<li class="dropdown language-switch">
    <a class="dropdown-toggle" data-toggle="dropdown">
        <img src="assets/images/flags/gb.png" class="position-left" alt="">
        English
        <span class="caret"></span>
    </a>

    <ul class="dropdown-menu">
        <li><a class="deutsch"><img src="assets/images/flags/de.png" alt=""> Deutsch</a></li>
        <li><a class="ukrainian"><img src="assets/images/flags/ua.png" alt=""> Українська</a></li>
        <li><a class="english"><img src="assets/images/flags/gb.png" alt=""> English</a></li>
        <li><a class="espana"><img src="assets/images/flags/es.png" alt=""> España</a></li>
        <li><a class="russian"><img src="assets/images/flags/ru.png" alt=""> Русский</a></li>
    </ul>
</li>