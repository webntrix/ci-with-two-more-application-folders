<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *  Custom helper is contain custom functions.
 * 
 * @author Harishkumar <harish.egnify@gmail.com>
 * 
 */
if (!function_exists('validate_data')) {

    /**
     *  validate_data function will validate the array data and add relevant Codes and Messages to the data
     * 
     * @param array $data is finalised array with key:value pairs return to the clainte.  
     * @param int $statusCode Based on the code the messages will appear. 200 default.
     * @return array is array with codes and messages to the clainte.
     */
    function validate_data($data, $statusCode) {
        $retData = array(
            'status' => '',
            'status_code' => $statusCode,
            'error' => '',
            'message' => '',
            'data_size' => sizeof($data),
            'data' => $data,
        );

        switch ($statusCode) {
            case 201:
                $retData['status'] = TRUE;
                $retData['error'] = FALSE;
                $retData['message_head'] = 'Save';
                $retData['message'] = "Data saving successfull.";
                break;

            case 202:
                $retData['status'] = TRUE;
                $retData['error'] = FALSE;
                $retData['message_head'] = 'Update';
                $retData['message'] = "Data updated successfully.";
                break;

            case 203:
                $retData['status'] = TRUE;
                $retData['error'] = FALSE;
                $retData['message_head'] = 'Delete';
                $retData['message'] = "Data deleted successfull";
                break;

            case 400:
                $retData['status'] = FALSE;
                $retData['error'] = TRUE;
                $retData['message_head'] = 'Bad Request';
                $retData['message'] = "Often missing a required parameter";
                break;

            case 401:
                $retData['status'] = FALSE;
                $retData['error'] = TRUE;
                $retData['message_head'] = 'Unauthorized';
                $retData['message'] = "No valid API key provided";
                break;

            case 402:
                $retData['status'] = FALSE;
                $retData['error'] = TRUE;
                $retData['message_head'] = 'Request Failed';
                $retData['message'] = "Parameters were valid but request failed";
                break;

            case 403:
                $retData['status'] = FALSE;
                $retData['error'] = TRUE;
                $retData['message_head'] = 'Forbidden';
                $retData['message'] = "The current API does not have access";
                break;

            case 404:
                $retData['status'] = FALSE;
                $retData['error'] = TRUE;
                $retData['message_head'] = 'Not Found';
                $retData['message'] = "The requested item doesn't exist";
                break;

            case 405:
                $retData['status'] = FALSE;
                $retData['error'] = TRUE;
                $retData['message_head'] = 'School not Found';
                $retData['message'] = "School Not Found";
                break;
            
            case 406:
                $retData['status'] = FALSE;
                $retData['error'] = TRUE;
                $retData['message_head'] = 'Data';
                $retData['message'] = "No data found";
                break;
            
            case 407:
                $retData['status'] = FALSE;
                $retData['error'] = TRUE;
                $retData['message_head'] = 'Data';
                $retData['message'] = "No valid data submited.";
                break;
            
            case 408:
                $retData['status'] = FALSE;
                $retData['error'] = TRUE;
                $retData['message_head'] = 'User';
                $retData['message'] = "Session expaired.";
                break;
            
            case 409:
                $retData['status'] = FALSE;
                $retData['error'] = TRUE;
                $retData['message_head'] = 'Authentication';
                $retData['message'] = "Authentication invalied.";
                break;

            case 500:
                $retData['status'] = FALSE;
                $retData['error'] = TRUE;
                $retData['message_head'] = 'Server errors';
                $retData['message'] = "Something went wrong. Please contact vendor if this mesage appers repetedly";
                break;

            default:
                $retData['status'] = TRUE;
                $retData['error'] = FALSE;
                $retData['message_head'] = "Success";
                $retData['message'] = "Successfull";
                break;
        }

        return $retData;
    }

}
if (!function_exists("loadSchoolDB")) {

    /**
     * Load logged in school DB dynamically.
     * @param string $dbName  Database name
     * @param string $userName  Database User name
     * @param string $password  Database Password
     * @return object
     */
    function loadSchoolDB($host, $database, $username, $password) {
        $dbConfig = array(
            'dsn' => '',
            'hostname' => $host,
            'username' => $username,
            'password' => $password,
            'database' => $database,
            'dbdriver' => 'mysqli',
            'dbprefix' => '',
            'pconnect' => FALSE,
            'db_debug' => (ENVIRONMENT !== 'production'),
            'cache_on' => FALSE,
            'cachedir' => '',
            'char_set' => 'utf8',
            'dbcollat' => 'utf8_general_ci',
            'swap_pre' => '',
            'encrypt' => FALSE,
            'compress' => FALSE,
            'stricton' => FALSE,
            'failover' => array(),
            'save_queries' => TRUE
        );

        $ci = & get_instance();
        return $ci->load->database($dbConfig, TRUE);
    }

}

if (!function_exists("getTablename")) {

    /**
     * Getting table name for doing CRUD activity
     * @param string $table is the keyword to get table name 
     * @return string 
     */
    function getTablename($table) {
        $tables = array(
            'users' => 'users',
            'sudents' => 'students'
        );
        return $tables[$table];
    }

}

if (!function_exists("printArray")) {

    /**
     * Print the raw formated data and its data type, and continue the execution
     * @param mixed $data printable data
     * @param bool $showDataType (optional) default FALSE: just print the $data. TRUE: Print the $data and it's data type, length.
     */
    function printArray($data, $showDataType = false) {
        echo "<pre>";
        if ($showDataType)
            var_dump($data);
        else
            print_r($data);
        echo "</pre>";
    }

}

if (!function_exists("printExit")) {

    /**
     * Print the raw formated data and its data type, and stop the execution
     * @param mixed $data printable data
     * @param bool $showDataType (optional) default FALSE: just print the $data. TRUE: Print the $data and it's data type, length.
     */
    function printExit($data, $showDataType = false) {
        echo "<pre>";
        if ($showDataType)
            var_dump($data);
        else
            print_r($data);
        echo "</pre>";
        exit;
    }

}
?>

