<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';

class Access extends REST_Controller {

    public function index_get($role="") {
        $data = array(
            "title" => $this->config->item('app_config')['title'].":Login",
            "copyright" => $this->config->item('app_config')['copyright'],
            "role" => $role
        );
        
        // checking for logo file 
        if(!empty($this->config->item('app_config')['logo']) && file_exists(substr(APPPATH, 0, strlen(APPPATH)-7)."assets/images/".$this->config->item('app_config')['logo'])){
            $data['logo'] = base_url("/assets/images/".$this->config->item('app_config')['logo']);
        }else{
            $data['logo'] = base_url("assets/images/logo.png");
        }
        
        $this->load->view('login_all',$data);
    }
    
    function api_post(){
        $this->load->model('access_model');
        $this->access_model->login($this->post());
        $this->response($this->post());
    }
}
