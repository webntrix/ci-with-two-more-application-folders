$(document).ready(function () {
    $("#btnSubmit").click(function () {
       $.ajax({
            type: 'POST',
            url: URL +"api",
            dataType: 'json',
            data: $("#loginform").serialize(),
            success: function($result){
                console.log($result);
            },
            error:function(errs, errCode){
                console.log(errs);
                console.log(errCode);
                alert(errCode + " :: " + errs );
            }
        });
    });
});