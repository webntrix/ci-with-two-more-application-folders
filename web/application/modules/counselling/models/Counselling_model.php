<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Counselling_model extends CI_Model {

    public function questions() {
        $QP = array(
            "Have you analyzed student's last week's Performance?" => array(
                "option" => array("yes", "No")
            ),
            
            "Do you think there is a scope for improvement in Student's performance?" => array(
                "option" => array(
                    "yes" => array(
                        "If Yes, What is the action you have proposed? " =>array(
                            "dropdown"=>array(
                                "Home Assignments",
                                "Attend Extra Classes",
                                "Work at Home/Hostel with Supervision",
                                "Visit Library for additional references",
                                "Get Support from Student Mentor",
                                "Other"
                            )
                        )
                    ),
                    "No"
                )
            ),
            
            "Do see any remarkable/abnormal changes in Student's performance?" => array(
                "option" => array(
                    "yes" => array(
                        "If Yes, What is the action you have proposed? " =>array(
                            "dropdown"=>array(
                                "Improvement in Punctuality",
                                "Worked on Take Home Assignments",
                                "Attending Extra Classes",
                                "Other"
                            )
                        )
                    ),
                    "No"
                )
            ),
            
            "Do see any remarkable/abnormal changes in Student's performance?" => array(
                "option" => array(
                    "yes" => array(
                        "If Yes, What is the action you have proposed? " =>array(
                            "dropdown"=>array(
                                "Improvement in Punctuality",
                                "Worked on Take Home Assignments",
                                "Attending Extra Classes",
                                "Other"
                            )
                        )
                    ),
                    "No"
                )
            ),
        );
    }

    public function adherence($filters=""){
        return "adherence";
    }
}
