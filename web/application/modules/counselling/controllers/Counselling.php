<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';

class Counselling extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
        
        $this->load->model(__CLASS__."_model");
    }

    public function index_get(){
        $this->template->set("title", "Counselling");
        $this->template->load("Counselling_view");
    }
    
    public function api_get($requestFor) {
        
        $this->response($rs);
    }
    
    public function api_post() {
        $this->crud->create("courses", $this->post());        
    }
    
    public function api_put($id) {
        $this->crud->update("courses", array("id"=>$id),$this->put());
    }
    
    public function api_delete($id) {
        $this->crud->delete("courses", array("id"=>$id));
    }
    

}
