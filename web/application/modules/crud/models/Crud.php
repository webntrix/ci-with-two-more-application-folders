<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Crud extends CI_Model {

    public function create($table, $data) {
        $this->db->query("SET FOREIGN_KEY_CHECKS = 0");
        $rs = $this->db->insert($table, $data);
        $this->db->query("SET FOREIGN_KEY_CHECKS = 1");
        printExit($rs);
        return true;
    }

    public function create_batch($table, $data) {
        $this->db->query("SET FOREIGN_KEY_CHECKS = 0");
        $this->db->insert_batch($table, $data);
        $this->db->query("SET FOREIGN_KEY_CHECKS = 1");
        return true;
    }

    public function read($table, $where = "", $fields = "", $start = 0, $limit = 0, $orderby = "", $order = "ASC", $groupby = "") {
        if (!empty($fields))
            $this->db->select($fields);

        if (!empty($where))
            $this->db->where($where);

        if ($limit > 0)
            $this->db->limit($start, $limit);

        if (!empty($orderby))
            $this->db->order_by($orderby, $order);

        if (!empty($groupby))
            $this->db->group_by($groupby);

        $rs = $this->db->get($table)->result_array();
        return $rs;
    }

    public function update($table, $where, $data) {
        $this->db->query("SET FOREIGN_KEY_CHECKS = 0");
        $this->db->update($table, $data, $where);
        $this->db->query("SET FOREIGN_KEY_CHECKS = 1");
        return true;
    }

    public function delete($table, $where) {
        $this->db->delete($table, $where);
        return true;
    }
    
    public function getfields(){
        $fields = $this->db->list_fields("student_faculty");
        return $fields;
    }

}
