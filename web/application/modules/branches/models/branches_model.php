<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Branches_model extends CI_Model {

    public function read() {
        $query = "select a.id, a.branch_code,a.course_code, b.course_name from branches as a "
                . "join courses as b on b.course_code = a.course_code";
        
        $rs = $this->db->query($query)->result_array();
        return $rs;
    }
        
}
