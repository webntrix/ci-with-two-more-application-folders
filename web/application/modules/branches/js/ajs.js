var app = angular.module("branches", []);

app.controller("ctrlBranches", function ($scope, $http, $filter) {
    $scope.save = function () {
        var httpHeads = {
            "Authenticate": "Basic",
            "X-API-KEY": "API KEY"
        };

        $http.post(document.location.href + "/api", $scope.branchData, httpHeads)
                .success(function (data, status, headers, config) {
                    $scope.PostDataResponse = data;
                    $scope.branchData.branch_code = undefined;
                    $scope.branchData.course_code = undefined;
                    $scope.refreshtabel();
                })
                .error(function (data, status, header, config) {

                })
    };

    $scope.refreshtabel = function () {
        $http({
            method: 'GET',
            url: document.location.href + "/api",
            //data: Object.toparams(myobject),
            headers: {
                "Content-Type": "application/json",
                "Authenticate": "Basic",
                "X-API-KEY": "API KEY"
            }
        }).then(
                function success(result) {
                    $scope.tblData = result.data;
                },
                function error() {}
        );
    };

    $scope.edit = function (id) {
        $scope.branchData = $filter('filter')($scope.tblData, {id: id})[0];
        $scope.btnsaveShow = false;
        $scope.btnupdateShow = true;
        ;
    };

    $scope.update = function () {
        var httpHeads = {
            "Authenticate": "Basic",
            "X-API-KEY": "API KEY"
        };

        $http.put(document.location.href + "/api/" + $scope.branchData.id, $scope.branchData, httpHeads)
                .success(function (data, status, headers, config) {
                    $scope.PostDataResponse = data;
                    $scope.branchData.branch_code = undefined;
                    $scope.branchData.course_code = undefined;
                    $scope.branchData.id = undefined;

                    $scope.btnsaveShow = true;
                    $scope.btnupdateShow = false;

                    $scope.refreshtabel();
                })
                .error(function (data, status, header, config) {

                })
    };
    $scope.deleteBranch = function (id) {
        $http({
            method: 'DELETE',
            url: document.location.href + "/api/" + id,
            //data: Object.toparams(myobject),
            headers: {
                "Content-Type": "application/json",
                "Authenticate": "Basic",
                "X-API-KEY": "API KEY"
            }
        }).then(
                function success(result) {
                    $scope.refreshtabel();
                },
                function error() {}
        );
    };

    $scope.getCoursecods = function () {
        $http({
            method: 'GETS',
            url: document.location.href + "/coursecodes",
            //data: Object.toparams(myobject),
            headers: {
                "Content-Type": "application/json",
                "Authenticate": "Basic",
                "X-API-KEY": "API KEY"
            }
        }).then(
                function success(result) {
                    $scope.coursecodes = result.data;
                },
                function error() {}
        );
    };
});