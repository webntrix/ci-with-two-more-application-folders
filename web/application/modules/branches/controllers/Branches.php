<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'libraries/REST_Controller.php';

class Branches extends REST_Controller{
    function __construct($config = 'rest') {
        parent::__construct($config);
        
        $this->load->model("crud/crud");
    }

    public function index_get(){
        $this->template->set("title", "Branches");
        $this->template->load("branches");
    }
    
    public function api_get() {
        $this->load->model("branches_model");
        $rs = $this->branches_model->read();
        $this->response($rs);
    }
    
    public function api_post() {
        $this->crud->create("branches", $this->post());        
    }
    
    public function api_put($id) {
        $this->crud->update("branches", array("id"=>$id),$this->put());
    }
    
    public function api_delete($id) {
        $this->crud->delete("branches", array("id"=>$id));
    }
    
    public function coursecodes_get(){
        $rs = $this->crud->read("courses","",array("id","course_name","course_code"));
        $this->response($rs);
    }
}

?>