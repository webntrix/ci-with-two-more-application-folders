<div ng-app="branches" ng-controller="ctrlBranches">
    <div class="panel panel-flat m-5">
    <div class="panel-heading no-margin p-5">
        <h3 class="no-margin"> <i class=" icon-stack2 position-left text-blue"></i> Branches</h3>
        <div class="heading-elements">
            <button class="btn btn-default heading-btn" id="btnrefresh" ng-click="refreshtabel()"><i class="icon-sync"></i></button>
        </div>
    </div>
</div>
<div class="panel panel-flat m-5">
    <div class="panel-body p-10">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label class="col-lg-3 control-label">Branch:</label>
                    <div class="col-lg-9">
                        <input type="text" name="branch_code" id="branch_code" class="form-control" placeholder="Branch code" ng-model="branchData.branch_code">
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label class="col-lg-3 control-label">Course:</label>
                    <div class="col-lg-9">
                        <select class="form-control" name="course_code" id="course_code" ng-init="getCoursecods()" ng-model="branchData.course_code"> 
                            <option value="" selected>- Select One -</option>
                            <option ng-repeat="cours in coursecodes" value="{{cours.course_code}}" >{{cours.course_name}}</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <button type="button" id="btnsave" name="btnsave" class="btn btn-primary width-100" ng-click="save()" ng-init="btnsaveShow=true" ng-show="btnsaveShow"><i class=" icon-floppy-disk"></i> Save</button>
                <button type="button" id="btnupdate" name="btnsave" class="btn btn-primary width-100" ng-click="update()" ng-init="btnupdateShow=false" ng-show="btnupdateShow"><i class=" icon-floppy-disk"></i> Save</button>
            </div>
        </div>
    </div>
</div>
<div class="panel panel-flat m-5">
        <table class="table table-hover datatable-highlight table-responsive" id="tblbranches" role="grid" >
            <thead>
                <tr role="row">
                    <th class="p-5">Branch</th>
                    <th class="p-5">Course </th>
                    <th class="p-5">&nbsp;</th>
                </tr>
            </thead>
            <tbody ng-init="refreshtabel()">
                <tr ng-repeat="row in tblData">
                    <td class="p-5">{{row.branch_code}}</td>
                    <td class="p-5">{{row.course_name}}</td>
                    <td class="p-5">
                        <button class="btn btn-xs btn-info" ng-click="edit(row.id)"><i class="icon-pencil"></i></button>
                        <button class="btn btn-xs btn-danger" ng-click="deleteBranch(row.id)"><i class="icon-trash"></i></button>
                    </td>
                </tr>
            </tbody>
        </table>
</div>
</div>
