<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'libraries/REST_Controller.php';

class Courses extends REST_Controller{
    function __construct($config = 'rest') {
        parent::__construct($config);
        
        $this->load->model("crud/crud");
    }

    public function index_get(){
        $this->template->set("title", "Courses");
        $this->template->load("courses");
    }
    
    public function api_get() {
        $rs = $this->crud->read("courses","",array("id","course_code","course_name","course_fullname"));
        $this->response($rs);
    }
    
    public function api_post() {
        $this->crud->create("courses", $this->post());        
    }
    
    public function api_put($id) {
        $this->crud->update("courses", array("id"=>$id),$this->put());
    }
    
    public function api_delete($id) {
        $this->crud->delete("courses", array("id"=>$id));
    }
    
}

?>