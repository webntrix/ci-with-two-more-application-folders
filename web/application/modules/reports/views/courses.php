<div ng-app="branches" ng-controller="ctrlBranches">
    <div class="panel panel-flat m-5">
    <div class="panel-heading no-margin p-5">
        <h3 class="no-margin"> <i class=" icon-stack2 position-left text-blue"></i> Courses</h3>
        <div class="heading-elements">
            <button class="btn btn-default heading-btn" id="btnrefresh" ng-click="refreshtabel()"><i class="icon-sync"></i></button>
        </div>
    </div>
</div>
<div class="panel panel-flat m-5">
    <div class="panel-body p-10">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label class="col-lg-4 control-label no-padding-left">Course name:</label>
                    <div class="col-lg-8 no-padding">
                        <input type="text" name="course_name" id="course_name" class="form-control" placeholder="Course name" ng-model="courseData.course_name">
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label class="col-lg-5 control-label no-padding-left">Course full name:</label>
                    <div class="col-lg-7 no-padding">
                        <input type="text" name="course_fullname" id="course_fullname" class="form-control" placeholder="Course fullname" ng-model="courseData.course_fullname">
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label class="col-lg-4 control-label no-padding-left">Course code:</label>
                    <div class="col-lg-8 no-padding">
                        <input type="text" name="course_code" id="course_code" class="form-control" placeholder="Course code" ng-model="courseData.course_code">
                    </div>
                </div>
            </div>
            
            <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12 center">
                <button type="button" id="btnsave" name="btnsave" class="btn btn-primary width-100" ng-click="save()" ng-init="btnsaveShow=true" ng-show="btnsaveShow"><i class=" icon-floppy-disk"></i> Save</button>
                <button type="button" id="btnupdate" name="btnsave" class="btn btn-primary width-100" ng-click="update()" ng-init="btnupdateShow=false" ng-show="btnupdateShow"><i class=" icon-floppy-disk"></i> Save</button>
            </div>
        </div>
    </div>
</div>
<div class="panel panel-flat m-5">
        <table class="table table-hover datatable-highlight table-responsive" id="tblbranches" role="grid" >
            <thead>
                <tr role="row">
                    <th class="p-5">Course Code</th>
                    <th class="p-5">Course Name </th>
                    <th class="p-5">Course Fullname </th>
                    <th class="p-5">&nbsp;</th>
                </tr>
            </thead>
            <tbody ng-init="refreshtabel()">
                <tr ng-repeat="row in tblData">
                    <td class="p-5">{{row.course_code}}</td>
                    <td class="p-5">{{row.course_name}}</td>
                    <td class="p-5">{{row.course_fullname}}</td>
                    <td class="p-5">
                        <button class="btn btn-xs btn-info" ng-click="edit(row.id)"><i class="icon-pencil"></i></button>
                        <button class="btn btn-xs btn-danger" ng-click="deleteBranch(row.id)"><i class="icon-trash"></i></button>
                    </td>
                </tr>
            </tbody>
        </table>
</div>
</div>
