<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Reports_model extends CI_Model {

    public function adherence($filters, $postData = "") {
        switch ($filters) {
            case "dept":
                $retrnData = $this->dept($postData);
                break;

            case "fac":
                $retrnData = $this->faculty($postData);
                break;

            default:
                $retrnData = $this->general($postData);
                break;
        }

        return $retrnData;
    }

    public function general($filers = "") {
        $ret_data = array();
        //get all councelling data 
        if (isset($filers['dept']) && !empty($filers['dept'])) {  //depend upon DEPT
            //get deparetment details
            $rsDept = $this->db->query("SELECT id, dept_shartname FROM departments WHERE id=" . $filers['dept'])->result_array();

            //get department wise facultys
            $rs = $this->db->query("SELECT DISTINCT(a.empcode) from faculty_details as a WHERE a.department_id=" . $filers['dept'])->result_array();

            $deptEmps = array();
            foreach ($rs as $row) {
                $deptEmps[] = $row['empcode'];
            }
            
            //get mapped students to the employees
            $rs = $this->db->query("SELECT empcode, students FROM faculty_students where empcode in (" . implode(",", (count($deptEmps) == 0 ? 0 : $deptEmps)) . ")")->result_array();
            $empStus = array();
            $stuString = "";
            foreach ($rs as $row) {
                $empStus[$row['empcode']][] = $row['students'];
                $stuString = (empty($stuString) ? "'" . $row['students'] . "'" : $stuString . ", '" . $row['students'] . "'");
            }

            //filter first year students
            $rs = $this->db->query("SELECT registerno FROM students_details where cyear=1 and registerno in (" . (empty($stuString) ? 0 : $stuString) . ")")->result_array();
            $mapStus = array();
            $mapStuString = "";
            foreach ($rs as $row) {
                $mapStus[] = $row['registerno'];
                $mapStuString = (empty($mapStuString) ? "'" . $row['registerno'] . "'" : $mapStuString . ", '" . $row['registerno'] . "'");
            }

            //find week wise data
            $rsWeeks = $this->db->query("SELECT DISTINCT(week_details) FROM `counselling_report` ")->result_array();
            $rsWeeksData = $this->db->query("SELECT DISTINCT(week_details), counselling_attendance,COUNT(counselling_attendance) as counts FROM `counselling_report` WHERE counselling_attendance!='' AND registerno IN(" . (empty($mapStuString) ? 0 : $mapStuString) . ") GROUP BY week_details,counselling_attendance ")->result_array();

            //sumarising data
            $rTemp = array();

            foreach ($rsWeeksData as $key => $value) {
                $rTemp[$value['week_details']]['week'] = $value['week_details'];
                $rTemp[$value['week_details']][$value['counselling_attendance']] = (int) $value['counts'];
            }
            $rsWeeks = array_reverse($rsWeeks, TRUE);
            $retTemp = array();

            foreach ($rsWeeks as $value) {
                if (isset($rTemp[$value['week_details']]))
                    $retTemp[$value['week_details']] = $rTemp[$value['week_details']];
                else
                    $retTemp[$value['week_details']]['week'] = $value['week_details'];
                if (!isset($retTemp[$value['week_details']]["Absent"]))
                    $retTemp[$value['week_details']]["Absent"] = 0;

                if (!isset($retTemp[$value['week_details']]["Present"]))
                    $retTemp[$value['week_details']]["Present"] = 0;
            }

            //pripare finaldata

            $ret_data['dept'] = (int) $filers['dept'];
            $ret_data['stu_count'] = count($mapStus);
            $ret_data['data'] = array_values($retTemp);
        }else {  //overall data            
            //filter first year students
            $rs = $this->db->query("SELECT registerno FROM students_details where cyear=1 ")->result_array();
            $mapStus = array();
            $mapStuString = "";
            foreach ($rs as $row) {
                //$mapStus[] = $row['registerno'];
                $mapStuString = (empty($mapStuString) ? "'" . $row['registerno'] . "'" : $mapStuString . ", '" . $row['registerno'] . "'");
            }


            //get mapped students to the employees
            $rs = $this->db->query("SELECT students FROM faculty_students where students in (" . (empty($mapStuString) ? 0 : $mapStuString) . ")")->result_array();
            $empStus = array();
            $stuString = "";
            foreach ($rs as $row) {
                $empStus[] = $row['students'];
                $stuString = (empty($stuString) ? "'" . $row['students'] . "'" : $stuString . ", '" . $row['students'] . "'");
            }
            
            //find week wise data
            $rsWeeks = $this->db->query("SELECT DISTINCT(week_details) FROM `counselling_report` ")->result_array();
            $rsWeeksData = $this->db->query("SELECT DISTINCT(week_details), counselling_attendance,COUNT(counselling_attendance) as counts FROM `counselling_report` WHERE counselling_attendance!='' AND registerno IN(" . (empty($stuString) ? 0 : $stuString) . ") GROUP BY week_details,counselling_attendance ")->result_array();

            //sumarising data
            $rTemp = array();
            foreach ($rsWeeksData as $key => $value) {
                $rTemp[$value['week_details']]['week'] = $value['week_details'];
                $rTemp[$value['week_details']][$value['counselling_attendance']] = (int) $value['counts'];
            }

            $rsWeeks = array_reverse($rsWeeks, TRUE);
            $retTemp = array();
            foreach ($rsWeeks as $value) {
                $retTemp[$value['week_details']] = $rTemp[$value['week_details']];

                if (!isset($retTemp[$value['week_details']]["Absent"]))
                    $retTemp[$value['week_details']]["Absent"] = 0;

                if (!isset($retTemp[$value['week_details']]["Present"]))
                    $retTemp[$value['week_details']]["Present"] = 0;
            }

            //pripare finaldata

            $ret_data['stu_count'] = count($empStus);
            $ret_data['data'] = array_values($retTemp);
        }


        return $ret_data;
    }

    public function dept($filers = "") {
        $ret_data = array();
        //get firstyear students
        $rs = $this->db->query("SELECT registerno FROM students_details where cyear=1 ")->result_array();
        $mapStus = array();
        $mapStuString = "";
        foreach ($rs as $row) {
            //$mapStus[] = $row['registerno'];
            $mapStuString = (empty($mapStuString) ? "'" . $row['registerno'] . "'" : $mapStuString . ", '" . $row['registerno'] . "'");
        }

        //get all mapped emps
        $rs = $this->db->query("SELECT DISTINCT(empcode) FROM faculty_students WHERE students in(" . $mapStuString . ")")->result_array();
        $mapEMP = array();
        foreach ($rs as $row) {
            $mapEMP[] = (int) $row['empcode'];
        }

        //get mapped students
        $rs = $this->db->query("SELECT empcode, students FROM faculty_students WHERE empcode in(" . implode(",", $mapEMP) . ") and students in (" . (empty($mapStuString) ? 0 : $mapStuString) . ")")->result_array();
        foreach ($rs as $row) {
            $mapStus[(int) $row['empcode']][] = $row['students'];
        }
        
        //get employee details
        $rs = $this->db->query("SELECT emp.id,emp.empcode,emp.emp_name, emp.department_id from faculty_details as emp WHERE emp.empcode IN (" . implode(",", $mapEMP) . ")")->result_array();
        $empData = array();
        foreach ($rs as $row) {
            $empData[$row['department_id']][$row['empcode']] = $row;
        }

        //get department details
        $rs = $this->db->query("SELECT dept.id, dept.dept_shartname FROM departments as dept WHERE dept.id IN (" . implode(",", array_keys($empData)) . ")")->result_array();
        $deptData = array();
        foreach ($rs as $row) {
            $deptData[$row['id']] = $row;
        }
        $baseRetdata = array();

        //get week data
        //get all weeks counts()
        $rs = $this->db->query("SELECT DISTINCT(week_details) from counselling_report")->result_array();
        $weekData = $rs;
        
        //check for the week filter
        if (isset($filers['week']) && !empty($filers['week'])) {
            $ret_data['student_count'] = $filers['week'];
            $query = "select "
                    . " counselling_report.empcode, counselling_report.counselling_attendance,count(counselling_report.counselling_attendance) as counts"
                    . " from counselling_report "
                    . " where counselling_report.counselling_attendance !='' and empcode in(" . implode(",", $mapEMP) . ") and week_details = '" . $filers['week'] . "'"
                    . " GROUP BY counselling_report.empcode, counselling_report.counselling_attendance";
        } else {
            $query = "select "
                    . " counselling_report.empcode, counselling_report.counselling_attendance,count(counselling_report.counselling_attendance) as counts"
                    . " from counselling_report "
                    . " where counselling_report.counselling_attendance !='' and empcode in(" . implode(",", $mapEMP) . ")"
                    . " GROUP BY counselling_report.empcode, counselling_report.counselling_attendance";
        }

        $rs = $this->db->query($query)->result_array();
        $attendence = array();
        foreach ($rs as $key => $value) {
            $attendence[(int) $value['empcode']][$value['counselling_attendance']] = $value['counts'];
        }

        $stuCount = 0;
        foreach ($empData as $dept => $emps) {
            $tot = 0;
            $p = 0;
            $a = 0;
            foreach ($emps as $key => $value) {
                $baseRetdata[$dept]['dept_id'] = (int) $deptData[$dept]['id'];
                $baseRetdata[$dept]['dept_shartname'] = $deptData[$dept]['dept_shartname'];
                $tot+=count($mapStus[$key]);

                if (isset($attendence[$key])) {
                    $a+= (isset($attendence[$key]['Absent']) ? $attendence[$key]['Absent'] : 0);
                    $p+= (isset($attendence[$key]['Present']) ? $attendence[$key]['Present'] : 0);
                }
            }
            $baseRetdata[$dept]['Absent'] = $a;
            $baseRetdata[$dept]['Present'] = $p;
            $baseRetdata[$dept]['student_count'] = ($tot *(count($weekData))) ;
            $stuCount += $tot;
        }
        
        $ret_data['week_count'] = count($weekData);
        $ret_data['student_count'] = $stuCount;
        $ret_data['data'] = array_values($baseRetdata);

        return $ret_data;
    }

    public function faculty($filers = "") {
        //check fo parameeters
        if ((isset($filers['week']) && empty($filers['week'])) || (isset($filers['dept']) && empty($filers['dept']))) {
            $ret_data['status'] = FALSE;
            $ret_data['message'] = "Unable to get data";
            return $ret_data;
        }

        //get firstyear students
        $rs = $this->db->query("SELECT registerno FROM students_details where cyear=1 ")->result_array();
        $mapStus = array();
        $mapStuString = "";
        foreach ($rs as $row) {
            //$mapStus[] = $row['registerno'];
            $mapStuString = (empty($mapStuString) ? "'" . $row['registerno'] . "'" : $mapStuString . ", '" . $row['registerno'] . "'");
        }

        //get all mapped emps
        $rs = $this->db->query("SELECT DISTINCT(empcode) FROM faculty_students WHERE students in(" . $mapStuString . ")")->result_array();
        $mapEMP = array();
        foreach ($rs as $row) {
            $mapEMP[] = (int) $row['empcode'];
        }
        
        //get dept employs
        $rs = $this->db->query("SELECT DISTINCT(empcode) FROM faculty_details WHERE empcode in(" . implode(",", $mapEMP) . ") and department_id = " . $filers['dept'])->result_array();
        $mapEMP = array();
        foreach ($rs as $row) {
            $mapEMP[] = (int) $row['empcode'];
        }

        //get mapped students
        $rs = $this->db->query("SELECT empcode, students FROM faculty_students WHERE empcode in(" . implode(",", $mapEMP) . ")")->result_array();
        foreach ($rs as $row) {
            $mapStus[(int) $row['empcode']][] = $row['students'];
        }
        //get employee details
        $rs = $this->db->query("SELECT emp.id,emp.empcode,emp.emp_name, emp.department_id from faculty_details as emp WHERE emp.empcode IN (" . implode(",", $mapEMP) . ") and emp.department_id = " . $filers['dept'])->result_array();
        $empData = array();
        foreach ($rs as $row) {
            $empData[$row['department_id']][$row['empcode']] = $row;
        }

        $query = "select "
                . " counselling_report.empcode, counselling_report.counselling_attendance,count(counselling_report.counselling_attendance) as counts"
                . " from counselling_report "
                . " where counselling_report.counselling_attendance !='' and empcode in(" . implode(",", $mapEMP) . ") and registerno in(" . $mapStuString . ") and week_details = '" . $filers['week'] . "'"
                . " GROUP BY counselling_report.empcode, counselling_report.counselling_attendance";

       
        $rs = $this->db->query($query)->result_array();
        $attendence = array();
        foreach ($rs as $key => $value) {
            $attendence[(int) $value['empcode']][$value['counselling_attendance']] = (int)$value['counts'];
        }
 //printExit($attendence);
        
        $stuCount = 0;
        foreach ($empData as $dept => $emps) {            
            foreach ($emps as $key => $value) {
                $baseRetdata[$key]['emp_id'] = (int) $value['id'];
                $baseRetdata[$key]['empcode'] = (int) $value['empcode'];
                $baseRetdata[$key]['emp_name'] = $value['emp_name'];
                $baseRetdata[$key]['student_count'] = count($mapStus[$key]);
                if (isset($attendence[$key])) {
                    $baseRetdata[$key]['Absent'] = (isset($attendence[$key]['Absent']) ? $attendence[$key]['Absent'] : 0);
                    $baseRetdata[$key]['Present'] = (isset($attendence[$key]['Present']) ? $attendence[$key]['Present'] : 0);
                } else {
                    $baseRetdata[$key]['Absent'] = 0;
                    $baseRetdata[$key]['Present'] = 0;
                }
                $stuCount += count($mapStus[$key]);
            }
        }
        
        $ret_data['student_count'] = $stuCount;
        $ret_data['week'] = $filers['week'];
        $ret_data['dept'] = (int) $filers['dept'];
        $ret_data['data'] = array_values($baseRetdata);
        return $ret_data;
    }

    public function liststudents($filters = array("cyear" => 1), $fields = "registerno,name,cyear,csemester") {
        $this->db->select($fields);
        if (!empty($filters))
            $this->db->where($filters);

        $rs = $this->db->get("students_details")->result_array();
        return $rs;
    }

    public function listfacultys($filters = "", $fields = "empcode, emp_name, department_id") {
        $this->db->select($fields);
        if (!empty($filters))
            $this->db->where($filters);

        $rs = $this->db->get("faculty_details")->result_array();
        return $rs;
    }

    public function listdepts($filters = "", $fields = "id, dept_shartname") {
        $this->db->select($fields);
        if (!empty($filters))
            $this->db->where($filters);

        $rs = $this->db->get("departments")->result_array();
        return $rs;
    }

    public function getmapping($filter = "") {
        $this->db->select("empcode,count(students) as mapped_students, group_concat(students) as students");
        if (!empty($filters))
            $this->db->where($filters);
        $this->db->group_by("empcode");
        $rs = $this->db->get("faculty_students")->result_array();
        return $rs;
    }

}
