<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Reports_model extends CI_Model {

    public function adherence($filters, $postData = "") {
        switch ($filters) {
            case "dept":
                $retrnData = $this->dept($postData);
                break;

            case "fac":
                $retrnData = $this->faculty($postData);
                break;

            default:
                $retrnData = $this->general($postData);
                break;
        }

        return $retrnData;
    }

    public function general($filers = "") {
        $ret_data = array();
        $rTemp = array();
        //get students
        $rsSTU1 = $this->db->query("select students,empcode from faculty_students")->result_array();
        $rsSTU2 = $this->db->query("select registerno from students_details where cyear = 1")->result_array();

        $stu1T = array();
        $emp1T = array();
        foreach ($rsSTU1 as $key => $value) {
            $stu1T[] = $value['students'];
            $emp1T[] = $value['empcode'];
        }
        $stu2T = array();
        foreach ($rsSTU2 as $key => $value) {
            $stu2T[] = $value['registerno'];
        }
        $stus = array_intersect($stu1T, $stu2T);
        $seucount = count($stus);

        $emp1T = array_unique($emp1T);

        if (isset($filers['dept']) && !empty($filers['dept'])) {
            $ret_data['dept'] = $filers['dept'];
            //get dept details
            $rs = $this->db->query("SELECT DISTINCT(`empcode`), emp_name FROM `faculty_details` where department_id = " . $filers['dept'])->result_array();
            $emp2T = array();
            foreach ($rs as $key => $value) {
                $emp2T[$value['empcode']] = $value['emp_name'];
            }
            $empResult = array_intersect($emp1T, array_keys($emp2T));
            $rs = $this->db->query("SELECT DISTINCT(`week_details`) FROM `counselling_report` where empcode in(" . implode(",", array_values($empResult)) . ")")->result_array();
            $rsWeeks = array_values(array_reverse($rs, TRUE));

            $rsPresent = $this->db->query("select week_details as `week`,count(id) as Present from `counselling_report` where counselling_attendance='Present' and empcode in(" . implode(",", array_values($empResult)) . ") group by week_details")->result_array();
            $rsAbsent = $this->db->query("select week_details as `week`,count(id) as Absent from `counselling_report` where counselling_attendance='Absent' and empcode in(" . implode(",", array_values($empResult)) . ") group by week_details")->result_array();

            $a = 0;
            foreach ($rsWeeks as $key => $week) {
                $a++;
                $rTemp[$a]['week'] = $week['week_details'];
                $pTemp = 0;
                foreach ($rsPresent as $presnetStu) {
                    if ($presnetStu['week'] == $week['week_details'])
                        $pTemp = $presnetStu['Present'];
                }

                $aTemp = 0;
                foreach ($rsAbsent as $absentStu) {
                    if ($absentStu['week'] == $week['week_details'])
                        $aTemp = $absentStu['Absent'];
                }

                $rTemp[$a]['present'] = $pTemp;
                $rTemp[$a]['absent'] = $aTemp;
            }
        } else {
            //getting week details
            $rs = $this->db->query("SELECT DISTINCT(`week_details`) FROM `counselling_report`")->result_array();
            $rsWeeks = array_values(array_reverse($rs, TRUE));

            $rsPresent = $this->db->query("select week_details as `week`,count(id) as Present from `counselling_report` where counselling_attendance='Present' group by week_details")->result_array();
            $rsAbsent = $this->db->query("select week_details as `week`,count(id) as Absent from `counselling_report` where counselling_attendance='Absent' group by week_details")->result_array();

            $a = 0;
            foreach ($rsWeeks as $key => $week) {
                $a++;
                $rTemp[$a]['week'] = $week['week_details'];
                $pTemp = 0;
                foreach ($rsPresent as $presnetStu) {
                    if ($presnetStu['week'] == $week['week_details'])
                        $pTemp = $presnetStu['Present'];
                }

                $aTemp = 0;
                foreach ($rsAbsent as $absentStu) {
                    if ($absentStu['week'] == $week['week_details'])
                        $aTemp = $absentStu['Absent'];
                }

                $rTemp[$a]['present'] = $pTemp;
                $rTemp[$a]['absent'] = $aTemp;
            }
        }

        $ret_data['stu_count'] = count($rsSTU2);
        $ret_data['data'] = array_values($rTemp);
        return $ret_data;
    }

    public function dept($filers = "") {
        // $rsCount = $this->db->query("select count(id) as count from counselling_report where week_details='" . $filers['week'] . "' ")->result_array();
        if (isset($filers['week']) && !empty($filers['week'])) {
            $rs = $this->db->query("select DISTINCT(a.empcode)as emps from counselling_report as a where week_details = '" . $filers['week'] . "'")->result_array();
        } else {
            $rs = $this->db->query("select DISTINCT(a.empcode)as emps from counselling_report as a ")->result_array();
        }

        $rsSTU1 = $this->db->query("select students,empcode from faculty_students")->result_array();
        $rsSTU2 = $this->db->query("select registerno from students_details where cyear = 1")->result_array();

        $stu1T = array();
        $emp1T = array();
        $empStus = array();
        foreach ($rsSTU1 as $key => $value) {
            $stu1T[] = $value['students'];
            $emp1T[] = (int) $value['empcode'];
            $empStus[$value['empcode']][] = $value['students'];
        }
        $emp1T = array_unique($emp1T);
        $stu2T = array();
        foreach ($rsSTU2 as $key => $value) {
            $stu2T[] = $value['registerno'];
        }
        $emp2T = array();
        foreach ($rs as $key => $emp) {
            $emp2T[] = (int) $emp['emps'];
        }


        $stus = array_intersect($stu1T, $stu2T);
        $emps = array_intersect($emp1T, $emp2T);
        $seucount = count($stus);
        $tmpFilter = "";
        foreach ($stus as $key => $value) {
            $tmpFilter = (empty($tmpFilter) ? "'" . $value . "'" : $tmpFilter . ",'" . $value . "'");
        }

        $rs1Yemp = $this->db->query("select DISTINCT(faculty_students.empcode) from faculty_students where faculty_students.students  in(" . $tmpFilter . ")")->result_array();
        $y1Emps = array();
        foreach ($rs1Yemp as $key => $value) {
            $y1Emps[] = (int) $value['empcode'];
        }

        $rs1Ydept = $this->db->query("select empcode, department_id from faculty_details where empcode  in(" . implode(",", $y1Emps) . ")")->result_array();
        $y1Depts = array();
        foreach ($rs1Ydept as $key => $value) {
            $y1Depts[$value['department_id']] = (int) $value['empcode'];
        }

        $rsdept = $this->db->query("SELECT faculty_details.department_id as dept, group_concat(faculty_details.empcode)as emp from faculty_details WHERE faculty_details.empcode in(" . implode(",", $y1Emps) . ") group by faculty_details.department_id")->result_array();
        $dept_emps = array();
        foreach ($rsdept as $key => $value) {
            $dept_emps[$value['dept']] = explode(",", $value['emp']);
        }
        $dept_emps2 = array();
        $where_emps = "";

        foreach ($dept_emps as $dept => $deptTeam) {
            $total = 0;
            foreach ($deptTeam as $key => $value) {
                $dept_emps2[$dept][$value] = (isset($empStus[$value]) ? count($empStus[$value]) : 0);
                $total+=(isset($empStus[$value]) ? count($empStus[$value]) : 0);
            }
            $dept_emps2[$dept]['total'] = $total;
        }

        foreach ($dept_emps2 as $dept => $deptTeam) {
            if (isset($deptTeam['total']))
                unset($deptTeam['total']);
            $where_emps = (empty($where_emps) ? (implode(",", array_keys($deptTeam))) : $where_emps . "," . (implode(",", array_keys($deptTeam))));
        }

        $dept = $this->db->query("SELECT id,dept_shartname from departments where id in (" . implode(",", array_keys($dept_emps2)) . ")")->result_array();

        //$rsStu = $this->db->query("select count(a.id)as assigned_students from faculty_students as a join students_details as stu on stu.registerno = a.students WHERE stu.cyear = 1")->result_array();
        if (isset($filers['week']) && !empty($filers['week'])) {
            $query = "select "
                    . " counselling_report.empcode, counselling_report.counselling_attendance,count(counselling_report.counselling_attendance) as counts"
                    . " from counselling_report "
                    . " where counselling_report.counselling_attendance !='' and empcode in(" . $where_emps . ") and week_details = '" . $filers['week'] . "'"
                    . " GROUP BY counselling_report.empcode, counselling_report.counselling_attendance";
        } else {
            $query = "select "
                    . " counselling_report.empcode, counselling_report.counselling_attendance,count(counselling_report.counselling_attendance) as counts"
                    . " from counselling_report "
                    . " where counselling_report.counselling_attendance !='' and empcode in(" . $where_emps . ")"
                    . " GROUP BY counselling_report.empcode, counselling_report.counselling_attendance";
        }
//        printExit($query);
        $rs = $this->db->query($query)->result_array();
        $pNa = array();
        foreach ($rs as $key => $value) {
            $pNa[(int) $value['empcode']][$value['counselling_attendance']] = $value['counts'];
        }

        foreach ($dept_emps as $deptKey => $deptTeam) {
            $dept_emps2[$deptKey]['Present'] = 0;
            $dept_emps2[$deptKey]['Absent'] = 0;
            foreach ($deptTeam as $key => $value) {
                $dept_emps2[$deptKey]['Present']+=(isset($pNa[$value]['Present']) ? (int) $pNa[$value]['Present'] : 0);
                $dept_emps2[$deptKey]['Absent']+=(isset($pNa[$value]['Absent']) ? (int) $pNa[$value]['Absent'] : 0);
            }
        }

        $retTemp = array();
        //printExit($dept_emps2);
        foreach ($dept as $deptArray) {
            $retTemp[$deptArray['dept_shartname']]["dept_id"] = (int) $deptArray['id'];
            $pTemp = 0;
            //foreach ($dept_emps2 as $empKey=>$presnetStu) {                
            $retTemp[$deptArray['dept_shartname']]['dept_shartname'] = $deptArray['dept_shartname'];

            //if ($empKey['id'] == $deptArray['id']) {
            $retTemp[$deptArray['dept_shartname']]['Absent'] = $dept_emps2[$deptArray['id']]['Absent'];
            $retTemp[$deptArray['dept_shartname']]['Present'] = $dept_emps2[$deptArray['id']]['Present'];
            $retTemp[$deptArray['dept_shartname']]['student_count'] = $dept_emps2[$deptArray['id']]['total'];
            //}
            //}
        }

        foreach ($retTemp as $dep => $deptArray) {
            if (!isset($deptArray['Absent']))
                $retTemp[$dep]['Absent'] = 0;

            if (!isset($deptArray['Present']))
                $retTemp[$dep]['Present'] = 0;
        }

        $ret_data['data_count'] = count($retTemp);
        $ret_data['student_count'] = $seucount;

        $ret_data['data'] = array_values($retTemp);
        //printExit($ret_data);
        return $ret_data;
    }

    public function faculty($filers = "") {
        //check fo parameeters
        if ((isset($filers['week']) && empty($filers['week'])) || (isset($filers['dept']) && empty($filers['dept']))) {
            $ret_data['status'] = FALSE;
            $ret_data['message'] = "Unable to get data";
            return $ret_data;
        }


        //get facultys from faculty udents
        $rs = $this->db->query(" select GROUP_CONCAT(DISTINCT(faculty_students.empcode)) as emps from faculty_students ")->result_array();
        $rsFac = $this->db->query("SELECT DISTINCT(faculty_details.empcode), faculty_details.emp_name FROM faculty_details where faculty_details.department_id=" . $filers['dept'] . " AND faculty_details.empcode IN (" . $rs[0]['emps'] . ")")->result_array();
        $facultys = 0;
        $afaculty = array();
        foreach ($rsFac as $fKey => $fValue) {
            $facultys = (empty($facultys) ? $fValue['empcode'] : $facultys . "," . $fValue['empcode'] );
            $afaculty[$fValue['empcode']] = $fValue['emp_name'];
        }

        $stus = $this->db->query("SELECT DISTINCT(counselling_report.registerno) as stus from counselling_report where counselling_report.empcode in (" . $facultys . ") ")->result_array();
        $mystu = 0;
        $mystu2 = 0;
        foreach ($stus as $key => $stu) {
            $mystu = (empty($mystu) ? "'" . $stu['stus'] . "'" : $mystu . ",'" . $stu['stus'] . "'");
        }
        $mapStus = $this->db->query("select registerno from students_details where cyear=1 and registerno in (" . $mystu . ")")->result_array();
        $mapStu = array();

        foreach ($mapStus as $key => $stu) {
            $mapStu[] = $stu['registerno'];
            $mystu2 = (empty($mystu2) ? "'" . $stu['registerno'] . "'" : $mystu2 . ",'" . $stu['registerno'] . "'");
        }
        $stuCount = $this->db->query("select count(id) as `count` from students_details where cyear=1 and registerno in (" . $mystu2 . ")")->result_array();

        $facStus = $this->db->query("select registerno from students_details where cyear=1 and registerno in (" . $mystu2 . ")")->result_array();
        $stus = "";
        foreach ($facStus as $key => $value) {
            $stus = (empty($stus) ? "'" . $value['registerno'] . "'" : $stus . ",'" . $value['registerno'] . "'");
        }
        $facStus = $this->db->query("select empcode, count(students) as stus from faculty_students where students in (" . $stus . ") group by empcode")->result_array();
        $empStus = array();
        foreach ($facStus as $key => $value) {
            $empStus[(int) $value['empcode']] = (int) $value['stus'];
        }
        if (isset($filers['week']) && !empty($filers['week'])) {
            $rsFacReult = $this->db->query("SELECT counselling_report.empcode, counselling_report.counselling_attendance, count(counselling_report.counselling_attendance) as `count` from counselling_report where counselling_report.registerno in (" . $mystu2 . ") and week_details='" . $filers['week'] . "' group by counselling_report.empcode, counselling_report.counselling_attendance ")->result_array();
            //$rsStucount = $this->db->query("SELECT counselling_report.empcode, counselling_report.counselling_attendance, count(counselling_report.counselling_attendance) as `count` from counselling_report where counselling_report.registerno in (" . $mystu2 . ") and week_details='" . $filers['week'] . "' group by counselling_report.empcode, counselling_report.counselling_attendance ")->result_array();
        } else {
            $rsFacReult = $this->db->query("SELECT counselling_report.empcode, counselling_report.counselling_attendance, count(counselling_report.counselling_attendance) as `count` from counselling_report where counselling_report.registerno in (" . $mystu2 . ") group by counselling_report.empcode, counselling_report.counselling_attendance ")->result_array();
            // $rsStucount = $this->db->query("SELECT counselling_report.empcode, counselling_report.counselling_attendance, count(counselling_report.counselling_attendance) as `count` from counselling_report where counselling_report.registerno in (" . $mystu2 . ") group by counselling_report.empcode, counselling_report.counselling_attendance ")->result_array();
        }
//        printExit($empStus);
        $retTemp = array();
        foreach ($facStus as $key => $value) {
            
        }

        foreach ($rsFacReult as $fKey => $fValue) {
            if (!empty($fValue['counselling_attendance']))
                $retTemp[$fValue['empcode']][$fValue['counselling_attendance']] = (int) $fValue['count'];
            //$retTemp[$fValue['empcode']][$fValue['counselling_attendance']] = (int) $fValue['count'];
        }

        foreach ($retTemp as $emp => $deptArray) {
            $retTemp[$emp]['emp_name'] = $afaculty[(int) $emp];
            $retTemp[$emp]['empcode'] = (int) $emp;
            $retTemp[$emp]['student_count'] = $empStus[(int) $emp];
            if (!isset($deptArray['Absent']))
                $retTemp[$emp]['Absent'] = 0;

            if (!isset($deptArray['Present']))
                $retTemp[$emp]['Present'] = 0;
        }

        //if ($rsCount[0]['count'] == 0) {
//        $ret_data['status'] = TRUE;
//        $ret_data['code'] = 200;
//        $ret_data['message'] = "Success";
//        $ret_data['data_count'] = count($retTemp);
        $ret_data['student_count'] = (int) $stuCount[0]['count'];
        $ret_data['week'] = $filers['week'];
        $ret_data['dept'] = (int) $filers['dept'];
        $ret_data['data'] = array_values($retTemp);
        return $ret_data;
//        } else {
//            
//        }
        //return $filers;
    }

    public function liststudents($filters = array("cyear" => 1), $fields = "registerno,name,cyear,csemester") {
        $this->db->select($fields);
        if (!empty($filters))
            $this->db->where($filters);

        $rs = $this->db->get("students_details")->result_array();
        return $rs;
    }

    public function listfacultys($filters = "", $fields = "empcode, emp_name, department_id") {
        $this->db->select($fields);
        if (!empty($filters))
            $this->db->where($filters);

        $rs = $this->db->get("faculty_details")->result_array();
        return $rs;
    }

    public function listdepts($filters = "", $fields = "id, dept_shartname") {
        $this->db->select($fields);
        if (!empty($filters))
            $this->db->where($filters);

        $rs = $this->db->get("departments")->result_array();
        return $rs;
    }

    public function getmapping($filter = "") {
        $this->db->select("empcode,count(students) as mapped_students, group_concat(students) as students");
        if (!empty($filters))
            $this->db->where($filters);
        $this->db->group_by("empcode");
        $rs = $this->db->get("faculty_students")->result_array();
        return $rs;
    }

}
