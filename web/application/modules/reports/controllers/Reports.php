<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'libraries/REST_Controller.php';

class Reports extends REST_Controller{
    function __construct($config = 'rest') {
        parent::__construct($config);
        
        $this->load->model("reports_model");
    }
//    public function index(){
//        echo "Welcome";
//    }

    public function index_get(){
        $this->template->set("title", "Reports");
        $this->template->load("reports");
    }
    
    public function api_get($report, $filter="") {   
        $rs = $this->getReport($report, $filter, $this->get());
        $this->response($rs);
    }
    
    public function api_post($report, $filter="") {
        $rs = $this->getReport($report, $filter, $this->post());
        $this->response($rs);   
    }
    
    
    private function getReport($report, $filters="", $postData=""){
        $rs = $this->reports_model->{$report}($filters,$postData);
        //printExit($rs);
        return $rs;
    }
    function test_get(){
        $rs = $this->reports_model->liststudents();
        $this->response($rs);
    }
    
}

?>